package org.acme.simplequarkusapps.loancalculator.controllers;

import io.smallrye.common.constraint.NotNull;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.acme.simplequarkusapps.configuration.handler.MessagesResponse;
import org.acme.simplequarkusapps.configuration.handler.ResponseMessage;
import org.acme.simplequarkusapps.loancalculator.services.payloads.LoanRequestMonthlyPayments;
import org.acme.simplequarkusapps.loancalculator.services.payloads.LoanRequestPrincipal;
import org.acme.simplequarkusapps.loancalculator.services.payloads.LoanRequestRate;
import org.acme.simplequarkusapps.loancalculator.services.LoanService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;


@Path("/loan")
@Tag(name = "Loan  - Calculator", description = "This will calculate interest in any time period given")
public class LoanControllerV4 {

    @Inject
    LoanService loanService;

    @POST
    @Path("/monthly-payments")
    @Operation(summary = "monthly-payments", description = "This will calculate interest in any time period given.")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateMonthlyPayments(LoanRequestMonthlyPayments request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                loanService.calculateMonthlyPayments(request))).build();    }
    @POST
    @Path("/rate-from-given-amount")
    @Operation(summary = "Rate from given Amount", description = "Rate from given Amount.")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateRateFromGivenAmount(@NotNull LoanRequestRate requestRate) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                loanService.calculateRateFromGivenAmount(requestRate))).build();
    }
    @POST
    @Path("/principal")
    @Operation(summary = "principal", description = "Rate from given Amount.")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculatePrincipal(@NotNull LoanRequestPrincipal request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                loanService.calculatePrincipal(request))).build();
    }

}
