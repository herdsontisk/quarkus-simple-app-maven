package org.acme.simplequarkusapps.vatcalculator.controllers;

import io.smallrye.common.constraint.NotNull;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.acme.simplequarkusapps.configuration.handler.MessagesResponse;
import org.acme.simplequarkusapps.configuration.handler.ResponseMessage;

import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestAmount;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestRate;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestTaxableAmount;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestTotalAmount;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.acme.simplequarkusapps.vatcalculator.services.VatService;



@Path("/vat")
@Tag(name = "VAT  - Calculator (calculates vat parameters)", description = "calculates vat")
public class VatController {

    @Inject
    VatService vatService;

    @GET
    @Path("/rates-and-info")
    @Operation(summary = "shows vat rates and info", description = "shows vat rates and info.")
    @APIResponse(description = "Successful", responseCode = "200")
    @Produces(MediaType.APPLICATION_JSON)
    public String vatInfo() {
        return vatService.vatRatesAndInfo();
    }

    @POST
    @Path("/amount")
    @Operation(summary = "calculates vat amount on goods and services", description = "calculates vat amount on goods and services in uganda.")
    @APIResponse(description = "Successful", responseCode = "200")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateVatAmount(@NotNull VatRequestAmount request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                vatService.calculateVatAmount(request))).build();
    }

    @POST
    @Path("/taxable-amount")
    @Operation(summary = "calculates Taxable amount/net amount before vat additions", description = "calculates Taxable amount before vat A mount additions.")
    @APIResponse(description = "Successful", responseCode = "200")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateTaxableAmount(@NotNull VatRequestTaxableAmount request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                vatService.calculateTaxableAmount(request))).build();
    }
    @POST
    @Path("/total-amount")
    @Operation(summary = "calculates Total amount/gross amount ie Taxable amount plus Vat Amount", description = "calculates total amount ie Taxable amount plus vat amount.")
    @APIResponse(description = "Successful", responseCode = "200")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateTotalAmount(@NotNull VatRequestTotalAmount request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                vatService.calculateTotalAmount(request))).build();
    }
    @POST
    @Path("/rate")
    @Operation(summary = "calculates the vat rate(%)", description = "calculates the vat rate(%) of the gross amount.")
    @APIResponse(description = "Successful", responseCode = "200")
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateVatRate(@NotNull VatRequestRate request) {
        return Response.ok(new ResponseMessage(MessagesResponse.SAVED.label,
                vatService.calculateVatRate(request))).build();
    }


}
