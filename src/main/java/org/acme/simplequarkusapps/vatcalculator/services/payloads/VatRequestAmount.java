package org.acme.simplequarkusapps.vatcalculator.services.payloads;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class VatRequestAmount {
    // vatAmount fields
    @Schema(required = true, example = "100000")
    private double taxableAmount;

    // constructors
    public void setTaxableAmount(double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }
    public double getTaxableAmount(){
        return taxableAmount;
    }


}
