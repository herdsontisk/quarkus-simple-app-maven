package org.acme.simplequarkusapps.vatcalculator.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import org.acme.simplequarkusapps.vatcalculator.models.*;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestAmount;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestRate;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestTaxableAmount;
import org.acme.simplequarkusapps.vatcalculator.services.payloads.VatRequestTotalAmount;

@ApplicationScoped
public class VatService {

    public Vat calculateVatAmount(VatRequestAmount request) {
        Vat vatObj = new Vat();
        vatObj.setTaxableAmount(request.getTaxableAmount());
        vatObj.calculateVatAmount();
        return vatObj;
    }

    public Vat calculateVatRate(VatRequestRate request) {
        Vat vatObj = new Vat();
        vatObj.setTaxableAmount(request.getTaxableAmount());
        vatObj.setVatAmount(request.getVatAmount());
        vatObj.calculateVatRate();
        return vatObj;
    }

    public Vat calculateTaxableAmount(VatRequestTaxableAmount request) {
        Vat vatObj = new Vat();
        vatObj.setVatAmount(request.getVatAmount());
        vatObj.calculateTaxableAmount();
        return vatObj;
    }

    public Vat calculateTotalAmount(VatRequestTotalAmount request) {
        Vat vatObj = new Vat();
        vatObj.setVatAmount(request.getVatAmount());
        vatObj.calculateTotalAmount();
        return vatObj;
    }

    public String vatRatesAndInfo() {
        Vat vatObj = new Vat();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Object json = objectMapper.createObjectNode().put("vatInfo", vatObj.getVatInfo());
            String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            System.out.println(jsonString);
            return jsonString;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
