package org.acme.simplequarkusapps.vatcalculator.services.payloads;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class VatRequestTotalAmount {
    // vatAmount fields
    @Schema(required = true, example = "10000")
    private double vatAmount;

    // constructors
    public void setVatAmount(double vatAmount) {
        this.vatAmount = vatAmount;
    }
    public double getVatAmount(){
        return vatAmount;
    }


}
