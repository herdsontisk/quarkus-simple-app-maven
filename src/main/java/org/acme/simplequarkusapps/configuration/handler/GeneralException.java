package org.acme.simplequarkusapps.configuration.handler;

import jakarta.ws.rs.WebApplicationException;


public class GeneralException {

	public static void JSONEncodingException(){
		throw new WebApplicationException("Server Error!",501);
	}
}
